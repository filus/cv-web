package com.novsys

import org.w3c.xhr.XMLHttpRequest

/**
 * @author fil.
 */
object Http {

    fun <R> get(url: String, success: (e: R?) -> dynamic) {
        request("get", url, null, success)
    }

    fun <R> post(url: String, data: Any?, success: (e: R?) -> dynamic) {
        request("post", url, data, success)
    }

    fun <R> request(method: String, url: String, data: Any?, success: (e: R?) -> dynamic) {
        val req = XMLHttpRequest()
        req.open(method, getBaseDomainName() + url)
        req.setRequestHeader("Content-Type", "application/json;charset=UTF-8")
        req.onreadystatechange = {
            if (req.readyState.equals(4)) {
                if (req.status.equals(200)) {
                    if (req.responseText.size > 0) {
                        success(JSON.parse<R>(req.responseText))
                    } else {
                        success(null)
                    }
                }
            }
        }
        if (data == null) {
            req.send()
        } else {
            req.send(JSON.stringify(data))
        }
    }

}