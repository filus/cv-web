package com.novsys

import jquery.*

fun getBaseDomainName(): String = js("getBaseDomainName()")

fun main(args: Array<String>) {
    jq() {
        Application().init()
    }
}