package com.novsys

import com.novsys.data.Mode
import com.novsys.data.Point
import com.novsys.data.Rect
import com.novsys.data.pointsToRect
import jquery.jq
import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement

/**
 * @author fil.
 */
class RectController(val cnv: HTMLCanvasElement) {

    private val cxt: CanvasRenderingContext2D? = cnv.getContext("2d") as? CanvasRenderingContext2D
    private var mul: Int = 1
    private var mode: Mode? = null
    private var drag = false
    private var downPoint: Point? = null
    private var rectList: MutableList<Rect> = mutableListOf()

    init {
        jq(cnv).on("mousedown", fun(e) {
            when (mode) {
                Mode.CONVEYOR_BORDER, Mode.BUFFERS, Mode.PALLET_COLOR_TRAINING -> {
                    drag = true
                    downPoint = Point(e.offsetX.toDouble(), e.offsetY.toDouble())
                }
                else -> {
                }
            }
        })
        jq(cnv).on("mouseup", fun(e) {
            drag = false
            if (mode != null && downPoint != null) {
                when (mode) {
                    Mode.CONVEYOR_BORDER -> {
                        val r = pointsToRect(downPoint!!.x, downPoint!!.y, e.offsetX.toDouble(), e.offsetY.toDouble())
                        if (rectList.size > 0) {
                            rectList[0] = r
                        } else {
                            rectList.add(r)
                        }
                        clear()
                        renderRectList()
                    }
                    Mode.BUFFERS, Mode.PALLET_COLOR_TRAINING -> {
                        rectList.add(pointsToRect(downPoint!!.x, downPoint!!.y,
                                e.offsetX.toDouble(), e.offsetY.toDouble()))
                        clear()
                        renderRectList()
                    }
                    else -> {
                    }
                }
            }
            downPoint = null
        })
        jq(cnv).on("mousemove", fun(e) {
            if (drag && downPoint != null) {
                clear()
                renderRectList()
                if (cxt != null) {
                    cxt.save()
                    cxt.strokeStyle = "red"
                    val rect = pointsToRect(downPoint!!.x, downPoint!!.y, e.offsetX.toDouble(), e.offsetY.toDouble())
                    cxt.strokeRect(rect.tl.x, rect.tl.y, rect.w, rect.h)
                    cxt.restore()
                }
            }
        })
    }

    fun setState(mode: Mode?) {
        this.mode = mode
    }

    fun setMul(value: Int) {
        this.mul = value;
    }

    fun clear() {
        cxt?.clearRect(.0, .0, cnv.width.toDouble(), cnv.height.toDouble())
    }

    fun renderRectList() {
        if (cxt != null) {
            cxt.save()
            cxt.strokeStyle = "red"
            for ((tl, w, h) in rectList) {
                cxt.strokeRect(tl.x, tl.y, w, h)
            }
            cxt.restore()
        }
    }

    fun getRectList(): MutableList<Rect> {
        val ret: MutableList<Rect> = mutableListOf()
        for ((tl, w, h) in rectList) {
            ret.add(Rect(Point(tl.x / mul, tl.y / mul), w / mul, h / mul))
        }
        return ret
    }

    fun release() {
        rectList.clear()
    }

}