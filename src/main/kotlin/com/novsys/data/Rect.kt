package com.novsys.data

/**
 * @author fil.
 */
data class Rect(var tl:Point, var w: Double, var h:Double)

fun pointsToRect(p1:Point, p2:Point):Rect{
    return pointsToRect(p1.x, p1.y, p2.x, p2.y)
}

fun pointsToRect(x1:Double, y1:Double, x2:Double, y2:Double):Rect{
    val tl = Point(Math.min(x1, x2), Math.min(y1, y2))
    val w = Math.abs(x1 - x2)
    val h = Math.abs(y1 - y2)
    return Rect(tl, w, h)
}