package com.novsys.data

/**
 * @author fil.
 */
data class Point(var x:Double, var y:Double)