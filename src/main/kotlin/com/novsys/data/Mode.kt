package com.novsys.data

/**
 * @author fil.
 */
enum class Mode {

    OUT_RECTANGLES,
    CONVEYOR_BORDER,
    BUFFERS,
    PALLET_COLOR_TRAINING

}