package com.novsys

import com.novsys.data.IntReq
import com.novsys.data.Mode
import com.novsys.data.ModeReq
import jquery.*
import org.w3c.dom.HTMLCanvasElement

@Suppress("UNUSED_PARAMETER")
/**
 * @author fil.
 */
class Application {

    lateinit var statePanel: JQuery
    lateinit var outContainer: JQuery
    lateinit var canvasContainer: HTMLCanvasElement
    lateinit var rectController: RectController
    var mode: Mode? = null
    var width = 0
    var height = 0
    var canvasMul = 1

    fun init() {
        jq {
            val container = jq("#container")
            container.append("<fieldset><legend>Status</legend><div id='statePanel' class='status-panel' /></fieldset>")
                    .append("<fieldset><legend>Action</legend><div id='buttonPanel' /></fieldset>")
                    .append("<fieldset><legend>Mode</legend><div id='modePanel' /></fieldset>")
                    .append("<div id='outContainer'>" +
                            "<img id='streamContainer' src='${getBaseDomainName()}/img?action=stream'>" +
                            "<canvas id='canvasContainer'></div>")

            outContainer = jq("#outContainer")
            canvasContainer = jq("#canvasContainer").get(0) as HTMLCanvasElement
            rectController = RectController(canvasContainer)

            statePanel = jq("#statePanel")
            jq("#buttonPanel")
                    .append("<input id='x1Btn' type='button' value='X1' class='btn btn-default'>")
                    .append("<input id='x2Btn' type='button' value='X2' class='btn btn-default'>")
                    .append("<input id='x3Btn' type='button' value='X3' class='btn btn-default'>")
                    .append("<input id='pauseBtn' type='button' value='pause' class='btn btn-primary'>")
                    .append("<input id='playBtn' type='button' value='play' class='btn btn-primary'>")
                    .append("<label style='margin-left: 10px'>go to</label>")
                    .append("<input id='gotoField' type='text' class='form-control goto'>")
                    .append("<input id='gotoBtn' type='button' value='>>' class='btn btn-primary'>")
                    .append("<label style='margin-left: 10px'>speed</label>")
                    .append("<input id='speedField' type='text' class='form-control goto'>")
                    .append("<input id='speedBtn' type='button' value='>>' class='btn btn-primary'>")
            jq("#modePanel")
                    .append("<input id='resetBtn' type='button' value='Clear' class='btn btn-danger'>")
                    .append("<input id='outRectBtn' type='button' value='Out rectangles' class='btn btn-primary'>")
                    .append("<input id='conveyorBtn' type='button' value='Conveyor border' class='btn btn-primary'>")
                    .append("<input id='buffersBtn' type='button' value='Work places' class='btn btn-primary'>")
                    .append("<input id='colorTrainingBtn' type='button' value='Color training' class='btn btn-primary'>")
                    .append("<input id='saveModeBtn' type='button' value='Save' class='btn btn-success'>")
            jq("#x1Btn").click { setCanvasSize(1) }
            jq("#x2Btn").click { setCanvasSize(2) }
            jq("#x3Btn").click { setCanvasSize(3) }
            jq("#pauseBtn").click { sendPause() }
            jq("#playBtn").click { sendPlay() }
            jq("#playBtn").click { sendPlay() }
            jq("#resetBtn").click { setMode(null) }
            jq("#saveModeBtn").click { saveMode() }
            jq("#outRectBtn").click { setMode(Mode.OUT_RECTANGLES) }
            jq("#conveyorBtn").click { setMode(Mode.CONVEYOR_BORDER) }
            jq("#buffersBtn").click { setMode(Mode.BUFFERS) }
            jq("#colorTrainingBtn").click { setMode(Mode.PALLET_COLOR_TRAINING) }

            // go to
            val gotoField = jq("#gotoField")
            gotoField.on("keypress", fun(e) {
                if (13.equals(e.keyCode)) {
                    goTo(parseInt(gotoField.`val`()))
                }
            })
            jq("#gotoBtn").click { goTo(parseInt(gotoField.`val`())) }

            // speed
            val speedField = jq("#speedField")
            speedField.on("keypress", fun(e) {
                if (13.equals(e.keyCode)) {
                    setSpeed(parseInt(speedField.`val`()))
                }
            })
            jq("#speedBtn").click { setSpeed(parseInt(speedField.`val`())) }

            reloadTrackerStatus()
        }
    }

    fun setCanvasSize(mul: Int) {
        canvasMul = mul
        rectController.setMul(mul)
        outContainer.css("width", "${width * mul}px")
        outContainer.css("height", "${height * mul}px")
        canvasContainer.width = width * mul
        canvasContainer.height = height * mul
    }

    fun reloadTrackerStatus() {
        Http.get("/tracker/status", fun(r: dynamic) {
            statePanel.html("<span>[ mode = ${r?.mode} ] [ pause = ${r?.pause} ]</span>")
            mode = if (r.mode == null) null else Mode.valueOf(r.mode)
            width = r.width
            height = r.height
            rectController.setState(mode)
            setCanvasSize(canvasMul)
        })
    }

    fun sendPause() {
        Http.post<Unit>("/tracker/pause", null, fun(r: Unit?) {
            reloadTrackerStatus()
        })
    }

    fun sendPlay() {
        Http.post<Unit>("/tracker/play", null, fun(r: Unit?) {
            reloadTrackerStatus()
        })
    }

    fun setMode(mode: Mode?) {
        Http.post<Unit>("/tracker/mode", ModeReq(mode?.name), fun(r: Unit?) {
            rectController.release()
            reloadTrackerStatus()
        })
    }

    fun saveMode() {
        when (mode) {
            Mode.CONVEYOR_BORDER -> {
                Http.post<Unit>("/tracker/conveyor_border", rectController.getRectList()[0], fun(r: Unit?) {
                    reloadTrackerStatus()
                    rectController.release()
                })
            }
            Mode.BUFFERS -> {
                Http.post<Unit>("/tracker/buffers", rectController.getRectList(), fun(r: Unit?) {
                    reloadTrackerStatus()
                    rectController.release()
                })
            }
            Mode.PALLET_COLOR_TRAINING -> {
                Http.post<Unit>("/tracker/color_training/start", rectController.getRectList(), fun(r: Unit?) {
                    reloadTrackerStatus()
                    rectController.release()
                })
            }
            else -> {
            }
        }
    }

    fun goTo(value: Int?) {
        if (value != null)
            Http.post<Unit>("/tracker/goto", IntReq(value), fun(r: Unit?) {
                reloadTrackerStatus()
            })
    }

    fun setSpeed(value: Int?) {
        if (value != null)
            Http.post<Unit>("/tracker/speed", IntReq(value), fun(r: Unit?) {
                reloadTrackerStatus()
            })
    }
}