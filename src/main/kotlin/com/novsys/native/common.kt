package com.novsys

import jquery.JQuery
import org.w3c.dom.Element

@native
open class Event() {
    val keyCode: Int
    val offsetX: Int
    val offsetY: Int
}

@native
fun JQuery.on(name: String, h: (e: Event) -> Any?): Unit = noImpl
@native
fun JQuery.css(name: String, value: String): Unit = noImpl
@native
fun JQuery.get(idx: Int): Element = noImpl

fun parseInt(str: String?): Int? {
    return if (str != null) safeParseInt(str) else null
}

fun parseLong(str: String?): Long? {
    return parseInt(str)?.toLong()
}